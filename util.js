const constants = require('cobox-constants')
function buildBaseURL (opts) {
  return ['http://', opts.hostname || 'localhost', ':', opts.port || constants.port, '/api'].join('')
}

module.exports = {
  buildBaseURL
}
