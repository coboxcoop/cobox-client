const debug = require('debug')('cobox-client')
const axios = require('axios')
const crypto = require('cobox-crypto')
const unionBy = require('lodash.unionby')
const { assign } = Object

const DEFAULT_ENDPOINT = 'https://localhost:9112/api'

class Client {
  constructor (opts = {}) {
    this.endpoint = opts.endpoint || DEFAULT_ENDPOINT
    this.id = opts.id || hex(crypto.randomBytes(2))
    debug(`create client: endpoint ${this.endpoint}`)
  }

  // Must specify a valid cobox resourceType
  // under the correct namespace, such as:
  // 'spaces', 'replicators', or ['admin', 'devices']
  async up (resourceType, params) {
    if (Array.isArray(resourceType)) var readableResource = resourceType.join(' ')
    else readableResource = resourceType

    let collection = await this.get(resourceType)

    if (!(params.name || params.address)) {
      if (!collection.length) return new Error(`you have no existing ${readableResource}`)
      let connections = await this.get([...resourceType, 'connections'])

      // get all those that aren't yet connected
      let remaining = unionBy(connections, collection, 'address')

      return await Promise.all([
        remaining.map((resource) => (
          this.post([...resourceType, resource.address, 'connections'])
        ))
      ])

      return collection
    } else {
      // check if resource already exists
      let resource = collection.find((resource) => (
        resource.name === params.name || resource.address === params.address
      ))

      // if so, open a new connection
      if (resource) return await this._request({
        method: 'POST',
        url: this._url([...resourceType, resource.address, 'connections'])
      })

      // if didn't exist, ensure name and address are provided
      if (!params.name && params.address) return new Error('you must provide an address')

      // create the new resource
      resource = await this._request({
        method: 'POST',
        url: this._url([resourceType]),
        data: params
      })

      // open a connection
      return await this._request({
        method: 'POST',
        url: this._url([...resourceType, resource.address, 'connections'])
      })
    }
  }

  // Must specify a valid cobox resourceType
  // under the correct namespace, such as:
  // 'spaces', 'replicators', or ['admin', 'devices']
  async down (resource) {

  }

  async get (resource, params, opts) {
    return this._request({
      method: 'GET',
      url: this._url(resource),
      params,
      ...opts
    })
  }

  async post (resource, params, data, opts) {
    return this._request({
      method: 'POST',
      url: this._url(resource),
      params,
      data,
      ...opts
    })
  }

  async put (resource, params, data, opts) {
    return this._request({
      method: 'PUT',
      url: this._url(resource),
      params,
      data,
      ...opts
    })
  }

  async patch (resource, params, data, opts) {
    return this._request({
      method: 'PATCH',
      url: this._url(resource),
      params,
      data,
      ...opts
    })
  }

  async delete (resource, params, opts) {
    return this._request({
      method: 'DELETE',
      url: this._url(resource),
      params,
      ...opts
    })
  }

  _url (path) {
    if (Array.isArray(path)) path = path.join('/')
    return this.endpoint + '/' + path
  }

  async _request (opts) {
    const axiosOpts = {
      method: opts.method || 'GET',
      url: opts.url || this._url(opts.path),
      maxRedirects: 0,
      timeout: 5000,
      headers: assign({ 'Content-Type': 'application/json' }, opts.headers || {}),
      data: opts.data || {},
      responseType: opts.responseType,
      params: opts.params
    }

    debug('request', axiosOpts)

    try {
      const response = await axios.request(axiosOpts)
      return response.data
    } catch (err) {
      debug(err)
      return err
    }
  }
}

module.exports = Client

function hex (b) {
  if (Buffer.isBuffer(b)) return b
  return b.toString('hex')
}
